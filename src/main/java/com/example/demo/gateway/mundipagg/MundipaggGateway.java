package com.example.demo.gateway.mundipagg;

import com.example.demo.Gateway;
import com.mundipagg.api.MundiAPIClient;
import org.springframework.stereotype.Service;

@Service
public class MundipaggGateway implements Gateway {

	private MundiAPIClient client;

	public MundipaggGateway(MundipaggConfig config) {
		this.client = new MundiAPIClient(config.getKey(), "");
	}
	
	public void authorize() {
		System.out.println("mundipagg authorize");
		
	}

	public void refund() {
		System.out.println("mundipagg refund");
		
	}

}
