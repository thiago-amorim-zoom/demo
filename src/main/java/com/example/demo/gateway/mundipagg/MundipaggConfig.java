package com.example.demo.gateway.mundipagg;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.example.demo.Gateway;
import com.mundipagg.api.MundiAPIClient;

@Component
public class MundipaggConfig {

	@Value("${mundipagg.key}")
	private String key;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}
}
