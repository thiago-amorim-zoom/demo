package com.example.demo;

public interface Gateway {

	public void authorize();
	public void refund();
}
